package me.tobi.ardacraft.registration.commands;

import java.util.Date;
import java.util.Set;

import me.tobi.ardacraft.api.ArdaCraftAPI;
import me.tobi.ardacraft.api.classes.Charakter;
import me.tobi.ardacraft.api.classes.Rank;
import me.tobi.ardacraft.api.classes.Rasse;
import me.tobi.ardacraft.api.classes.User;
import me.tobi.ardacraft.api.classes.Utils;
import me.tobi.ardacraft.api.classes.Utils.Attitude;
import me.tobi.ardacraft.api.message.FancyMessage;
import me.tobi.ardacraft.registration.CharakterManager;
import me.tobi.ardacraft.registration.RegistrationDialog;
import me.tobi.ardacraft.registration.generator.GeneratorOrk;
import me.tobi.ardacraft.registration.generator.GeneratorZwerg;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdCharacter implements CommandExecutor {

	//character show <Volk>
	//character set <Player> <Character> <ignoreDate?> <ignoreTaken?>
	//character add <Name> <Volk> <Hauptcharakter?>

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(sender instanceof Player) {
			Player psend = (Player)sender;
			if(args.length == 0) {
				psend.sendMessage("§cBenutzung: /character <show|set|add|reset|info>");
				return true;
			}
			if(args[0].equalsIgnoreCase("show")) {
				if(args.length == 2) {
					Rasse r = null;
					try {
						r = Rasse.valueOf(args[1].toUpperCase());
					}catch(Exception e) {
						psend.sendMessage("§cBitte gib ein gültiges Volk an!");
						return true;
					}
					if(Charakter.get(psend) == null) {
						new RegistrationDialog(psend).showCharacters(r, false);
					}else {
						new RegistrationDialog(psend).showCharacters(r, true);
					}
					//nur falls spieler UNREGSITERED oder kann cahrakter ändern
				}else {
					psend.sendMessage("§cBenutzung: /character show <Volk>");
				}
			}else if(args[0].equalsIgnoreCase("set")) {
				//System.out.println("SET");
				if (args.length == 4 || args.length == 5) {
					//System.out.println("length: 4 || 5");
					if (Rank.get(psend) == Rank.SPIELER) {
						Player pchange = Bukkit.getPlayer(args[1]);
						//System.out.println("player is not a teammember");
						Boolean ignoreDate = false;
						Boolean ignoreTaken = false;
						//System.out.println("no exception");
						Charakter target = ArdaCraftAPI.getACDatabase().getCharacterManager().get(args[2]);
						if(target == null) {
							psend.sendMessage("§cDieser Charakter existiert nicht!");
							return true;
						}
						if(target.isMain() && Charakter.get(pchange) == null) {
							//System.out.println("erste charakterwahl, hauptcharakter");
							psend.sendMessage("§cDu (bzw. " + pchange.getName() + ") darfst noch keinen Haupt-Charakter Spielen.");
							return true;
						}
						if (Charakter.get(pchange) == null) {
							//System.out.println("player char is null");
							if (!target.isTaken()) {
								//System.out.println("target char not taken");
								if (!target.isMain()) {
									//System.out.println("target char not main");
									CharakterManager.setCharakter(pchange, target);
								} else {
									psend.sendMessage("§cDu (bzw. " + pchange.getName() + ") kannst erst später Hauptcharaktere wählen!");
								}
							} else {
								psend.sendMessage("§cDieser Charakter wird bereits verwendet!");
							}
						} else {
							Charakter cha = Charakter.get(pchange);
							if ((!target.isTaken()) || ignoreTaken) {
								//System.out.println("taken: " + target.isTaken() + "; ignore taken: " + ignoreTaken);
								//TODO Test
								boolean dateValid = new Date().getTime() - ArdaCraftAPI.getACDatabase().getUserManager().get(psend.getUniqueId().toString()).getDateCharacterChanged() > 7700000000L;
								if (dateValid || ignoreDate) { // 3 monate =
									// 7700000000L
									//System.out.println("dateValid: " + dateValid + "; ignoreDate: " + ignoreDate);
									//System.out.println(new Date());
									//System.out.println(cha.getDate());
									//System.out.println(System.currentTimeMillis());
									//System.out.println(cha.getDate().getTime());
									//System.out.println(new Date(0));
									CharakterManager.setCharakter(pchange, target);
								} else {
									psend.sendMessage("§cDu (bzw. " + pchange.getName() + ") kannst deinen Charakter nur alle drei Monate Wechseln!");
									long days = ((((7700000000L - (new Date().getTime() - ArdaCraftAPI.getACDatabase().getUserManager().get(psend.getUniqueId().toString()).getDateCharacterChanged())) / 1000)/ 60)/60)/24;
									psend.sendMessage("§cWarte noch " + days + " tage.");
								}
							} else {
								psend.sendMessage("§cDieser Charakter wird bereits verwendet!");
							}
						}


					} else { //TEAM

						Player pchange = Bukkit.getPlayer(args[1]);
						//System.out.println("player is team");
						Boolean ignoreDate = false;
						Boolean ignoreTaken = false;
						try {
							ignoreDate = Boolean.valueOf(args[3]);
							ignoreTaken = Boolean.valueOf(args[4]);
						} catch (Exception e) {
							//System.out.println("Error");
							psend.sendMessage("§cBitte gib entweder 'true' oder 'false' an!");
							return true;
						}
						//System.out.println("no exception");
						Charakter target = ArdaCraftAPI.getACDatabase().getCharacterManager().get(args[2]);
						if(target == null) {
							psend.sendMessage("§cDieser Charakter existiert nicht! Du kannst ihn mit /charakter add <Name> hinzufügen.");
							return true;
						}
						if (Charakter.get(pchange) == null) {
							//System.out.println("player char is null");
							if (!target.isTaken()) {
								//System.out.println("target char not taken");
								if (!target.isMain()) {
									//System.out.println("target char not main");
									CharakterManager.setCharakter(pchange, target);
								} else {
									psend.sendMessage("§cDu (bzw. " + pchange.getName() + ") kannst erst später Hauptcharaktere wählen!");
								}
							} else {
								psend.sendMessage("§cDieser Charakter wird bereits verwendet!");
							}
						} else {
							Charakter cha = Charakter.get(pchange);
							if ((!target.isTaken()) || ignoreTaken) {
								//System.out.println("taken: " + target.isTaken() + "; ignore taken: " + ignoreTaken);
								boolean dateValid = new Date().getTime() - ArdaCraftAPI.getACDatabase().getUserManager().get(psend.getUniqueId().toString()).getDateCharacterChanged() > 7700000000L;
								if (dateValid || ignoreDate) { // 3 monate =
									// 7700000000L
									//System.out.println("dateValid: " + dateValid + "; ignoreDate: " + ignoreDate);
									//System.out.println(new Date());
									//System.out.println(cha.getDate());
									//System.out.println(System.currentTimeMillis());
									//System.out.println(cha.getDate().getTime());
									//System.out.println(new Date(0));
									if(cha.isTaken() && ignoreTaken) {
										CharakterManager.removeCharakter(User.getByUUID(pchange.getUniqueId().toString()));
									}
									CharakterManager.setCharakter(pchange, target);
								} else {
									psend.sendMessage("§cDu (bzw. " + pchange.getName() + ") kannst deinen Charakter nur alle drei Monate Wechseln!");
									long days = ((((7700000000L - (new Date().getTime() - User.getByUUID(psend.getUniqueId().toString()).getDateCharacterChanged())) / 1000)/ 60)/60)/24;
									psend.sendMessage("§cWarte noch " + days + " tage.");
								}
							} else {
								psend.sendMessage("§cDieser Charakter wird bereits verwendet!");
							}
						}

					}
					/*
					 * Charakter c =
					 * ArdaCraftAPI.getACDatabase().getCharacter(args[2]);
					 * if(c.getPlayerUUID() != null) {
					 * if(args[3].equalsIgnoreCase("true")) { new
					 * CharakterManager(p).setCharakter(c, true); }else {
					 * p.sendMessage
					 * ("Dieser Charakter wird bereits verwendet. Um den " +
					 * "Charakter zu überschreiben verwende /character set <Player> "
					 * + "<Charakter> true."); } }else { new
					 * CharakterManager(p).setCharakter(c, false); }
					 */
				}else {
					psend.sendMessage("§cBenutzung: /character set <Player> <Charakter> <ignoreLastChanged> <ignoreTaken>");
				}
			}else if(args[0].equalsIgnoreCase("add")) {
				if(Rank.get(psend).isHigherThen(Rank.SPIELER)) {
					if(args.length == 4) {
						if(args[2].equalsIgnoreCase("true") || args[3].equalsIgnoreCase("false")) {
							if(ArdaCraftAPI.getACDatabase().getCharacterManager().get(args[1]) == null) {
								Rasse r = null;
								try {
									r = Rasse.valueOf(args[2].toUpperCase());
								}catch(Exception e) {
									psend.sendMessage("§cBitte gib ein gültiges Volk an!");
									return true;
								}
								boolean main = Boolean.valueOf(args[3]);
								ArdaCraftAPI.getACDatabase().getCharacterManager().add(new Charakter(ArdaCraftAPI.getACDatabase().getCharacterManager().getAll().size(), args[1], r, main));
								psend.sendMessage("§aCharakter §e" + args[1] + " §aerfolgreich hinzugefügt!");
							}else {
								psend.sendMessage("§cDer Charakter existiert bereits!");
							}
						}else {
							psend.sendMessage("§cBitte gib entweder 'true' oder 'false' an!");
						}
					}else {
						psend.sendMessage("§cBenutzung: /character add <Name> <Volk> <Hauptcharakter?>");
					}
				}else {
					psend.sendMessage("§cDieser Befehl ist nur für Teammitglieder!");
				}
			/*}else if(args[0].equalsIgnoreCase("remove")) { //character remove <Charakter>
				if(Rank.get(p).isHigherThen(Rank.SPIELER)) {
					if(args.length == 2) {
						Charakter target = ArdaCraftAPI.getACDatabase().getCharakter(args[1]);
						if(target.getPlayerUUID() != "null") {
							Player targetP = Bukkit.getPlayer(target.getPlayerUUID());
							CharakterManager manager = new CharakterManager(targetP);
							manager.removeCharakter();
						}
						ArdaCraftAPI.getACDatabase().removeCharakter(target);
						p.sendMessage("§aDer Charakter wurde erfolgreich entfernt.");
					}else {
						p.sendMessage("§cBenutzung: /character remove <Charakter>");
					}
				}else {
					p.sendMessage("§cDieser Befehl ist nur für Teammitglieder!");
				}*/
			}else if(args[0].equalsIgnoreCase("reset")) { //character reset <Player>
				if(Rank.get(psend).isHigherThen(Rank.SPIELER)) {
					if(args.length == 2) {
						Charakter c = ArdaCraftAPI.getACDatabase().getCharacterManager().get(args[1]);
						if(c == null) {
							psend.sendMessage("§cDieser Charakter existiert nicht!");
							return true;
						}
						CharakterManager.removeCharakter(User.getByName(args[0]));
						psend.sendMessage("§aDu hast den Charakter von" + c.getName() + " resettet!");
					}else {
						psend.sendMessage("§cBenutzung: /character reset <Spieler>");
					}
				}else {
					psend.sendMessage("§cDieser Befehl ist nur für Teammitglieder!");
				}
			}else if (args[0].equalsIgnoreCase("info")) {
				Player p = (Player)sender;
				if(args.length == 2) {
					Charakter c = ArdaCraftAPI.getACDatabase().getCharacterManager().get(args[1]);
					if(c == null) {
						p.sendMessage("§cDieser Charakter existiert nicht!");
					}else {
						p.sendMessage("§eCharakter Info: " + c.getName());
						String rasse = c.getRasse().getAttitude().equals(Attitude.GOOD)?"§a" + c.getRasse().toString():"§7" + c.getRasse();
						rasse = Utils.normalize(rasse);
						p.sendMessage("§aVolk: " + rasse);
						String main = c.isMain()?"§aJa":"§cNein";
						p.sendMessage("§aHauptcharakter: " + main);
						String belegt = c.isTaken()?"§aJa, von " + CharakterManager.getUserForCharacter(c).getLastName() + " seit " + new Date(CharakterManager.getUserForCharacter(c).getDateCharacterChanged()).toString():"§cNein";
						p.sendMessage("§aBelegt: " + belegt);
					}
				}else {
					p.sendMessage("§cBenutzung: /character info <Charaktername>");
				}
			}else if (args[0].equalsIgnoreCase("get")) {
				Player send = (Player)sender;
				if(args.length == 2) {
					User u = User.getByName(args[1]);
					if(u != null) {
						Charakter c =u.getCharakter();
						if(c != null) {
							send.sendMessage("§a" + u.getLastName() + " spielt als " + c.getName());
						}else {
							send.sendMessage("§cDieser Spieler hat keinen Charakter!");
						}
					}else {
						send.sendMessage("§cDieser Spieler existiert nicht!");
					}
				}
			}else if (args[0].equalsIgnoreCase("clearbanned")) {
				Player p = (Player)sender;
				if(Rank.get(p).isHigherThen(Rank.MOD)) {
					int resettet = 0;
					//TODO Test new solution
					Set<OfflinePlayer> i = Bukkit.getServer().getBannedPlayers();
					for(OfflinePlayer bannedPlayer : i) {
						User u = User.getByName(bannedPlayer.getName());
						if(u != null) {
                            CharakterManager.removeCharakter(u);
                            System.out.println("[CHARAKTER] Resetting charakter " + u.getCharakter().getName() + " from " + u.getLastName());
                            resettet++;

						}
					}
					p.sendMessage("§aEs wurden erfolgreich " + resettet + " Charaktere resettet!");
				}
			}else if (args[0].equalsIgnoreCase("generate")) { //character generate <Volk>
				Player p = (Player)sender;
				if(Rank.get(p).isHigherThen(Rank.MOD)) {
					if(args.length == 2) {
						try{
							Rasse r = Rasse.valueOf(args[1].toUpperCase());
							FancyMessage msg = new FancyMessage("");
							if(r == Rasse.ORK) {
								msg.then("§eOrk Namen: ");
								GeneratorOrk generator = new GeneratorOrk();
								for(int i = 0;i < 20; i++) {
									String s = generator.generate();
									msg.then("§a" + s).command("/character add " + s + " ork false").then("§r, ");
								}
							}else if(r == Rasse.ZWERG) {
								msg.then("§eZwerg Namen: ");
								GeneratorZwerg generator = new GeneratorZwerg();
								for(int i = 0;i < 20; i++) {
									String s = generator.generate();
									msg.then("§a" + s).command("/character add " + s + " zwerg false").then("§r, ");
								}
							}else {
								p.sendMessage("§cFür dieses Volk ist noch kein Generator verfügbar!");
							}
							Utils.sendJsonMessage(msg.toJSONString(), p);
						}catch(Exception e) {
							p.sendMessage("§cBitte gib ein gültiges Volk an!");
						}
					}else {
						p.sendMessage("§cBenutzung: /character generate <Volk>");
					}
				}
			}else {
				psend.sendMessage("§cBenutzung: /character <show|set|add|reset|info>");
			}
		}else {
			System.out.println("Dieser Befehl ist nur für Spieler");
		}
		return true;
	}

}

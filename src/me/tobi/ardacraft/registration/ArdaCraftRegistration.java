package me.tobi.ardacraft.registration;

import me.tobi.ardacraft.registration.commands.CmdCharacter;

import org.bukkit.plugin.java.JavaPlugin;

public class ArdaCraftRegistration extends JavaPlugin{
	
	@Override
	public void onEnable() {
		Data.writeToDatabase();
		
		getCommand("character").setExecutor(new CmdCharacter());
	}
	
}

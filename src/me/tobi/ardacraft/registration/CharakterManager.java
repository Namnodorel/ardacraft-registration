package me.tobi.ardacraft.registration;

import me.tobi.ardacraft.api.ArdaCraftAPI;
import me.tobi.ardacraft.api.classes.Charakter;
import me.tobi.ardacraft.api.classes.User;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;

import java.util.Date;

public class CharakterManager {
	
	public static void setCharakter(Player p, Charakter c) {
		if(Charakter.get(p) == null) {
			/*if(c.getRasse().getAttitude() == Attitude.GOOD) {
				p.teleport(new Location(Bukkit.getWorld("RPG"), 5548, 71, -3520));
			}else {
				p.teleport(new Location(Bukkit.getWorld("RPG"), 1860, 72, 2534));
			}*/
		}else {
			removeCharakter(User.getByName(p.getName()));
		}
		for(PotionEffect pe : p.getActivePotionEffects()) {
			p.removePotionEffect(pe.getType());
		}
		//PermissionUser user = PermissionsEx.getUser(p);
		//user.setPrefix(Utils.normalize(c.getRasse().toString()), null);
		//System.out.println(Utils.normalize(c.getRasse().toString()));
		//System.out.println("Date before change was " + c.getDate());
		User u = User.getByUUID(p.getUniqueId().toString());
        u.setDateCharacterChanged(new Date().getTime());
		u.setCharakter(c);
		ArdaCraftAPI.getACDatabase().getUserManager().update(u);
		//System.out.println("Set time for " + c.getName() + " to " + c.getDate().getTime());
		//System.out.println("Time from database: " + Charakter.get(p).getDate().getTime());
		p.sendMessage("§aDu spielst jetzt als " + c.getName());
		p.getServer().broadcastMessage("§e" + p.getName() + "§a spielt jetzt als §e" + c.getName() + "§a!");
	}

	public static void removeCharakter(User u) {

        u.setCharakter(null);

        ArdaCraftAPI.getACDatabase().getUserManager().delete(u.getUUID());
        ArdaCraftAPI.getACDatabase().getUserManager().add(u);
	}

	public static User getUserForCharacter(Charakter c){
        for(User u : ArdaCraftAPI.getACDatabase().getUserManager().getAll()){
            if(u.getCharakter().getName().equals(c.getName())){
                return u;
            }
        }

        return null;
    }


}
